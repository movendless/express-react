var express = require('express');
var helpers = require('./helpers/helpers');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');

// Database
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var db = mongoose.connection;

// Schemas
var Movie = require('./models/movie');

// Routes
var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
var hbs;
hbs = exphbs.create({
   extname: '.hbs',
   defaultLayout: 'layout',
   helpers: helpers
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// Database install
mongoose.connect('mongodb://localhost/social-movie');

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log('Yay! Db ok!');
});

//Database create functions
var newMovie = new Movie({
  name: 'Interstellar',
  imdb: 8.7     ,
  location: 'USA',
  meta: {
    genres: ['Science fiction', 'Action', 'Adventure', 'Drama', 'Mystery'],
    website: 'blu-ray.com'
  },
  date: "2015/03/25"
});

console.log("film: " + newMovie.name + " Imdb Puan: " + newMovie.imdb + " Lokasyon: " + newMovie.location + " Tür/Türler:" + newMovie.meta.genres);
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}
    
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
